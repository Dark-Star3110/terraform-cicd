variable "region" {
  default = "ap-northeast-1"
}

variable "instance_type" {
  default = "t2.micro"
}